%Matlab 2011a script
%download data from imgw and wttr sites
%
%author: AdrianKobyl
%date: 20-07-2019
%version: 1.2


%AD.1
addpath('C:\Users\student\Desktop\JSON\jsonlab') % json implementation

%AD8

% create file with column names
fileName = 'weather_data.csv';
header = {'temperatura','cinienie','prdko wiatru','wilgotno wzgldna','miasto','godzina pomiaru','data pomiaru'};
headerUnits = {'C', 'hPa','m/s', '%'};
dataFileId = fopen('nowe.csv','w+');
fprintf(dataFileId,'%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s\n', '"',header{1},'";"',header{2},'";"',header{3},'";"',header{4},'";"',header{5},'";"',header{6},'";"',header{7},'"');
fprintf(dataFileId,'%s%s%s%s%s%s%s%s%s\n', '"',headerUnits{1},'";"',headerUnits{2},'";"',headerUnits{3},'";"',headerUnits{4},'"');
fclose(dataFileId);



% while ( ) {

dane_wttr_raciborz=loadjson(urlread ('http://wttr.in/raciborz?format=j1')); %all data loaded
dane_imgw_raciborz=loadjson(urlread('https://danepubliczne.imgw.pl/api/data/synop/station/raciborz/format/json')); %all data loaded

dane_wttr_katowice=loadjson(urlread ('http://wttr.in/katowice?format=j1'));%all data loaded
dane_imgw_katowice=loadjson(urlread('https://danepubliczne.imgw.pl/api/data/synop/station/katowice/format/json')); %all data loaded

dane_wttr_czestochowa=loadjson(urlread ('http://wttr.in/czestochowa?format=j1')); %all data loaded
dane_imgw_czestochowa=loadjson(urlread('https://danepubliczne.imgw.pl/api/data/synop/station/czestochowa/format/json')); %all data loaded



dane_wttr_raciborz_obecne=dane_wttr_raciborz.current_condition; %variable with current indications
dane_wttr_raciborz_obecne_mat=cell2mat(dane_wttr_raciborz_obecne);%available data in the form of a matrix

tem_wttr_raciborz_obecne=str2num(dane_wttr_raciborz_obecne_mat.temp_C); %variable with temperature as number
cis_wttr_raciborz_obecne=str2num(dane_wttr_raciborz_obecne_mat. pressure); %variable with pressure as number
vwiatr_wttr_raciborz_obecne=str2num(dane_wttr_raciborz_obecne_mat.windspeedKmph); %variable with wind speed as number
wilgotnosc_wttr_raciborz_obecne=str2num(dane_wttr_raciborz_obecne_mat.humidity); %variable with humidity as number
czas_wttr_raciborz_obecne=dane_wttr_raciborz_obecne_mat.observation_time;
dane_wttr_r=[tem_wttr_raciborz_obecne,cis_wttr_raciborz_obecne,vwiatr_wttr_raciborz_obecne,wilgotnosc_wttr_raciborz_obecne]
a=cell2mat(dane_wttr_raciborz.weather);
data_wttr_raciborz= a.date;

dataFileId = fopen('weather_data','a+');
fprintf(dataFileId,'%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s\n', '"',' ','";"',' ','";"',' ','";"',' ','";"','Raciborz','";"',czas_wttr_raciborz_obecne,'";"',data_wttr_raciborz,'"');
fclose(dataFileId);

dlmwrite('weather_data',dane_wttr_r, '-append','delimiter', ';');


temp_imgw_raciborz=str2num(dane_imgw_raciborz.temperatura);  
cis_imgw_raciborz=str2num(dane_imgw_raciborz.cisnienie); 
vwiatr_imgw_raciborz=str2num(dane_imgw_raciborz.predkosc_wiatru); 
wilgotnosc_imgw_raciborz=str2num(dane_imgw_raciborz.wilgotnosc_wzgledna); 
czas_imgw_raciborz=dane_imgw_raciborz.godzina_pomiaru; 
data_imgw_raciborz=dane_imgw_raciborz.data_pomiaru;
dane_imgw_r=[temp_imgw_raciborz,cis_imgw_raciborz,vwiatr_imgw_raciborz,wilgotnosc_imgw_raciborz];

dataFileId = fopen('weather_data','a+');
fprintf(dataFileId,'%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s\n', '"',' ','";"',' ','";"',' ','";"',' ','";"','Raciborz','";"',czas_imgw_raciborz,'";"',data_imgw_raciborz,'"');
fclose(dataFileId);

dlmwrite('weather_data.csv',dane_imgw_r, '-append','delimiter', ';');
 

dane_wttr_katowice_obecne=dane_wttr_katowice.current_condition; 
dane_wttr_katowice_obecne_mat=cell2mat(dane_wttr_katowice_obecne);

tem_wttr_katowice_obecne=str2num(dane_wttr_katowice_obecne_mat.temp_C);
cis_wttr_katowice_obecne=str2num(dane_wttr_katowice_obecne_mat. pressure);
vwiatr_wttr_katowice_obecne=str2num(dane_wttr_katowice_obecne_mat.windspeedKmph);
wilgotnosc_wttr_katowice_obecne=str2num(dane_wttr_katowice_obecne_mat.humidity);
czas_wttr_katowice_obecne=dane_wttr_katowice_obecne_mat.observation_time;
dane_wttr_k=[tem_wttr_katowice_obecne,cis_wttr_katowice_obecne,vwiatr_wttr_katowice_obecne,wilgotnosc_wttr_katowice_obecne]
b=cell2mat(dane_wttr_katowice.weather);
data_wttr_katowice= b.date;

dataFileId = fopen('weather_data','a+');
fprintf(dataFileId,'%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s\n', '"',' ','";"',' ','";"',' ','";"',' ','";"','katowice','";"',czas_wttr_katowice_obecne,'";"',data_wttr_katowice,'"');
fclose(dataFileId);

dlmwrite('weather_data.csv',dane_wttr_k, '-append','delimiter', ';');

temp_imgw_katowice=str2num(dane_imgw_katowice.temperatura); 
cis_imgw_katowice=str2num(dane_imgw_katowice.cisnienie); 
vwiatr_imgw_katowice=str2num(dane_imgw_katowice.predkosc_wiatru); 
wilgotnosc_imgw_katowice=str2num(dane_imgw_katowice.wilgotnosc_wzgledna); 
czas_imgw_katowice=dane_imgw_katowice.godzina_pomiaru; 
data_imgw_katowice=dane_imgw_katowice.data_pomiaru;
dane_imgw_k=[temp_imgw_katowice,cis_imgw_katowice,vwiatr_imgw_katowice,wilgotnosc_imgw_katowice];

dataFileId = fopen('weather_data','a+');
fprintf(dataFileId,'%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s\n', '"',' ','";"',' ','";"',' ','";"',' ','";"','katowice','";"',czas_imgw_katowice,'";"',data_imgw_katowice,'"');
fclose(dataFileId);

dlmwrite('weather_data.csv',dane_imgw_k, '-append','delimiter', ';');


dane_wttr_czestochowa_obecne=dane_wttr_czestochowa.current_condition; 
dane_wttr_czestochowa_obecne_mat=cell2mat(dane_wttr_czestochowa_obecne);

tem_wttr_czestochowa_obecne=str2num(dane_wttr_czestochowa_obecne_mat.temp_C);
cis_wttr_czestochowa_obecne=str2num(dane_wttr_czestochowa_obecne_mat. pressure);
vwiatr_wttr_czestochowa_obecne=str2num(dane_wttr_czestochowa_obecne_mat.windspeedKmph);
wilgotnosc_wttr_czestochowa_obecne=str2num(dane_wttr_czestochowa_obecne_mat.humidity);
czas_wttr_czestochowa_obecne=dane_wttr_czestochowa_obecne_mat.observation_time;
dane_wttr_cz=[tem_wttr_czestochowa_obecne,cis_wttr_czestochowa_obecne,vwiatr_wttr_czestochowa_obecne,wilgotnosc_wttr_czestochowa_obecne]
c=cell2mat(dane_wttr_czestochowa.weather);
data_wttr_czestochowa= c.date;

dataFileId = fopen('weather_data','a+');
fprintf(dataFileId,'%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s\n', '"',' ','";"',' ','";"',' ','";"',' ','";"','czestochowa','";"',czas_wttr_czestochowa_obecne,'";"',data_wttr_czestochowa,'"');
fclose(dataFileId);

dlmwrite('weather_data.csv',dane_wttr_cz, '-append','delimiter', ';');


temp_imgw_czestochowa=str2num(dane_imgw_czestochowa.temperatura); 
cis_imgw_czestochowa=str2num(dane_imgw_czestochowa.cisnienie); 
vwiatr_imgw_czestochowa=str2num(dane_imgw_czestochowa.predkosc_wiatru); 
wilgotnosc_imgw_czestochowa=str2num(dane_imgw_czestochowa.wilgotnosc_wzgledna)
czas_imgw_czestochowa=dane_imgw_czestochowa.godzina_pomiaru; 
data_imgw_czestochowa=dane_imgw_czestochowa.data_pomiaru;
dane_imgw_cz=[temp_imgw_czestochowa,cis_imgw_czestochowa,vwiatr_imgw_czestochowa,wilgotnosc_imgw_czestochowa];

dataFileId = fopen('weather_data','a+');
fprintf(dataFileId,'%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s\n', '"',' ','";"',' ','";"',' ','";"',' ','";"','czestochowa','";"',czas_imgw_czestochowa,'";"',data_imgw_czestochowa,'"');
fclose(dataFileId);

dlmwrite('weather_data.csv',dane_imgw_cz, '-append','delimiter', ';');

%}