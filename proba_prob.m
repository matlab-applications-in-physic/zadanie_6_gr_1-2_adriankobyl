%Matlab 2011a script
%download data from imgw and wttr sites
%
%author: AdrianKobyl
%date: 20-07-2019
%version: 1.2


%AD.1
addpath('C:\Users\student\Desktop\JSON\jsonlab') %implementacja json'a

%AD8
%a) 
dane_wttr_raciborz=loadjson(urlread ('http://wttr.in/raciborz?format=j1'));%wczytuje dane z tej strony ale wszystkie
%
dane_wttr_raciborz_obecne=dane_wttr_raciborz.current_condition; %zmienna z obecnymi wskazaniami
dane_wttr_raciborz_obecne_mat=cell2mat(dane_wttr_raciborz_obecne);%obecne dane w formie macierzy

tem_wttr_raciborz_obecne=str2num(dane_wttr_raciborz_obecne_mat.temp_C);
cis_wttr_raciborz_obecne=str2num(dane_wttr_raciborz_obecne_mat. pressure);
vwiatr_wttr_raciborz_obecne=str2num(dane_wttr_raciborz_obecne_mat.windspeedKmph);
wilgotnosc_wttr_raciborz_obecne=str2num(dane_wttr_raciborz_obecne_mat.humidity);
czas_wttr_raciborz_obecne=dane_wttr_raciborz_obecne_mat.observation_time;
dane_wttr_r=[tem_wttr_raciborz_obecne,cis_wttr_raciborz_obecne,vwiatr_wttr_raciborz_obecne,wilgotnosc_wttr_raciborz_obecne]
a=cell2mat(dane_wttr_raciborz.weather);%pomocnicze
data_wttr_raciborz= a.date;



fileName = 'nowe_2.csv';
header = {'temperatura','ci�nienie','pr�dko�� wiatru','wilgotno�� wzgl�dna','miasto','godzina pomiaru','data pomiaru'};
headerUnits = {'�C', 'hPa','m/s', '%'};
dataFileId = fopen('nowe.csv','w+');
fprintf(dataFileId,'%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s\n', '"',header{1},'";"',header{2},'";"',header{3},'";"',header{4},'";"',header{5},'";"',header{6},'";"',header{7},'"');
fprintf(dataFileId,'%s%s%s%s%s%s%s%s%s\n', '"',headerUnits{1},'";"',headerUnits{2},'";"',headerUnits{3},'";"',headerUnits{4},'"');
fclose(dataFileId)
dataFileId = fopen('nowe_2.csv','a+');
fprintf(dataFileId,'%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s\n', '"',' ','";"',' ','";"',' ','";"',' ','";"','Raciborz wttr','";"',czas_wttr_raciborz_obecne,'";"',data_wttr_raciborz,'"');
%fclose(dataFileId)%->nie wiem kaj cie da�;
%dataFileId = fopen(fileName,'w+') ma by�: 'a+' �eby dodawalo
%odtad w p�tli 

dlmwrite('nowe_2.csv',dane_wttr_r, '-append','delimiter', ';');
fclose(dataFileId);




dane_imgw_raciborz=loadjson(urlread('https://danepubliczne.imgw.pl/api/data/synop/station/raciborz/format/json'));
%wczytuje obecne dane z racibora 
temp_imgw_raciborz=str2num(dane_imgw_raciborz.temperatura); %temp 
cis_imgw_raciborz=str2num(dane_imgw_raciborz.cisnienie); %cisnienie
vwiatr_imgw_raciborz=str2num(dane_imgw_raciborz.predkosc_wiatru); %predkosc wiatru
wilgotnosc_imgw_raciborz=str2num(dane_imgw_raciborz.wilgotnosc_wzgledna); %wilgotnosc
czas_imgw_raciborz=dane_imgw_raciborz.godzina_pomiaru; %godzina pomiarow
data_imgw_raciborz=dane_imgw_raciborz.data_pomiaru;
dane_imgw_r=[temp_imgw_raciborz,cis_imgw_raciborz,vwiatr_imgw_raciborz,wilgotnosc_imgw_raciborz];

%proba generalna
dataFileId = fopen('nowe_2.csv','a+');
fprintf(dataFileId,'%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s\n', '"',' ','";"',' ','";"',' ','";"',' ','";"','Raciborz imgw','";"',czas_imgw_raciborz,'";"',data_imgw_raciborz,'"');
%fclose(dataFileId);
dlmwrite('nowe_2.csv',dane_imgw_r, '-append','delimiter', ';');
fclose(dataFileId);





